#!/bin/sh

PACKAGE=$(dpkg-parsechangelog -S Source)
VERSION=$2
TAR=../$PACKAGE_$VERSION.orig.tar.gz
TAG=jdk7u79-b15

rm -f $3

wget http://hg.openjdk.java.net/jdk7u/jdk7u/hotspot/archive/$TAG.tar.gz -O $TAR
